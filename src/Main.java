import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLOutput;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.DataFormatException;

/*
#########################################################################################
# -This is a template source code file given you to complete 4th quiz work.             #
#                                                                                       #
# -You are strongly expected to declare and define data structure and recursive         #
# method within the comment-lines dedicated below.                                      #
#                                                                                       #
# -DO-NOT modify the comment-lines (not even a letter in those lines) that determine    #
# the boundaries of the blocks in which you are to perform the expected behaviors.      #
# In the case you modify you are likely to get 0 points from this quiz!                 #
#                                                                                       #
# -You can expand the lines of these blocks as much as you wish.                        #
#                                                                                       #
# -DO-NOT rename this file!                                                             #
# -DO-NOT use any class file other than this!                                           #
# -Feel free to define functions as much as you wish but within this class!             #
#########################################################################################
*/
public class Main {
    public static void main(String[] args) {

        // Declare all the data structure(s) below that you need
        // DO-NOT spoil the inside of the block with the code-snippets other than declarations!
        /////////////// DATA-STRUCTURE DECLARATION: BEGIN ///////////////
        Map<String, String> nonTerminalSymbolToExpansionMap = new HashMap<>();
        /////////////// DATA-STRUCTURE DECLARATION: END   ///////////////

        try {
            BufferedReader reader = new BufferedReader(new FileReader(args[0]));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] duple = line.split("->");
                String nonTerminalSymbol = duple[0];
                String expansion = duple[1];
                nonTerminalSymbolToExpansionMap.put(nonTerminalSymbol, expansion);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        String toBeExpanded = String.format("(%s)", nonTerminalSymbolToExpansionMap.get("S"));

        System.out.println(expand(toBeExpanded, nonTerminalSymbolToExpansionMap));
    }

    // Define and fill recursive method below
    // Note: The whole structure of the recursion (including the begin-end curly braces)
    // should be placed within the block
    /////////////// RECURSION: BEGIN ///////////////
    private static String expand(String string, Map<String, String> nonTerminalSymbolToExpansionMap) {
        for (String symbol : nonTerminalSymbolToExpansionMap.keySet()) {
            if (string.contains(symbol)) {
                return expand(string.replace(symbol, String.format("(%s)", nonTerminalSymbolToExpansionMap.get(symbol))), nonTerminalSymbolToExpansionMap);
            }
        }
        return string;
    }
    /////////////// RECURSION: END   ///////////////
}